FROM python:latest

RUN apt-get update
RUN apt-get install git jq libgeos-dev postgresql-client -y
RUN pip install --upgrade pip

RUN wget -O pgfutter https://github.com/lukasmartinelli/pgfutter/releases/download/v1.2/pgfutter_linux_amd64 && chmod +x pgfutter
# make sure pfgutter is available.
RUN cp pgfutter /usr/local/bin
